import React, {useEffect, useRef, useState} from 'react';
import {
  Animated,
  Dimensions,
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import * as d3Shape from 'd3-shape';
import color from 'randomcolor';
import Svg, {G, Path, Text, TSpan} from 'react-native-svg';
import MyText from './components/MyText';

const {width, height} = Dimensions.get('screen');
const numberOfSegments = 7;
const wheelSize = width * 0.8;
const fontSize = 26;
const oneTurn = 360;
const angleBySegment = oneTurn / numberOfSegments;
const angleOffSet = angleBySegment / 2;

const makeWheel = () => {
  const data = Array.from({length: numberOfSegments}).fill(1);
  const arcs = d3Shape.pie()(data);
  const colors = color({
    luminosity: 'dark',
    count: numberOfSegments,
  });
  const value = ['Quay Tiếp', '100', '200', '300', '400', '500', '600'];
  return arcs.map((arc, index) => {
    const instance = d3Shape
      .arc()
      .padAngle(0.01)
      .outerRadius(width / 2)
      .innerRadius(20);
    return {
      path: instance(arc),
      color: colors[index],
      value: value[index],
      centroid: instance.centroid(arc),
    };
  });
};
const App = () => {
  const wheelPaths = makeWheel();
  const [_angle, setAngle] = useState(new Animated.Value(0));
  const [enabled, setEnabled] = useState(false);
  const [finished, setFinished] = useState(false);
  const [started, setStarted] = useState(false);
  const [winner, setWinner] = useState(null);
  const [point, setPoint] = useState(0);
  let _winner = Math.floor(Math.random() * numberOfSegments);
  const angle = useRef(0);

  useEffect(() => {
    _angleListener();
  }, []);

  const _angleListener = () => {
    _angle.addListener(event => {
      if (enabled) {
        setEnabled(false);
        setFinished(false);
      }
      angle.current = event.value;
    });
  };
  const getWinner = () => {
    const deg = Math.abs(Math.round(angle.current % oneTurn));

    if (angle.current < 0) {
      return Math.floor(deg / angleBySegment);
    }
    return (
      (numberOfSegments - Math.floor(deg / angleBySegment)) % numberOfSegments
    );
  };

  const RenderWheel = () => {
    return (
      <Animated.View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          transform: [
            {
              rotate: _angle.interpolate({
                inputRange: [-oneTurn, 0, oneTurn],
                outputRange: [`-${oneTurn}deg`, '0deg', `${oneTurn}deg`],
              }),
            },
          ],
        }}>
        <Svg
          width={wheelSize}
          height={wheelSize}
          viewBox={`0 0 ${width} ${width}`}
          style={{transform: [{rotate: `-${angleOffSet}deg`}]}}>
          <G y={width / 2} x={width / 2}>
            {wheelPaths.map((arc, index) => {
              const [x, y] = arc.centroid;
              const val = arc.value.toString().toUpperCase();
              return (
                <G key={`arc-${index}`}>
                  <Path d={arc.path} fill={arc.color} />
                  <G
                    rotation={
                      (index * oneTurn) / numberOfSegments + angleOffSet
                    }
                    origin={`${x}, ${y}`}>
                    <Text
                      x={x}
                      y={y - 70}
                      fill="#fff"
                      textAnchor="middle"
                      fontSize={fontSize}>
                      <TSpan x={x} dy={fontSize}>
                        {val}
                      </TSpan>
                    </Text>
                  </G>
                </G>
              );
            })}
          </G>
        </Svg>
      </Animated.View>
    );
  };

  const onPress = (preIndex = 0) => {
    const duration = 2000;
    setStarted(true);
    _angle.setValue(0);
    let presetRoundCounter = duration/100+Math.floor(Math.random()*10) // angleBySegment*(duration/100+Math.floor(Math.random()*10))
    let presetToValue = angleBySegment*presetRoundCounter
    if (preIndex === 0 && presetRoundCounter%7 === 0) {
      presetToValue = presetToValue + angleBySegment
    }

    Animated.timing(_angle, {
      toValue: presetToValue,
      duration: duration,
      useNativeDriver: true,
    }).start(() => {
      const winnerIndex = getWinner();
      if (winnerIndex !== 0) {
        setPoint(parseInt(point) + parseInt(wheelPaths[winnerIndex].value));
      } else {
        onPress(winnerIndex);
      }
    });
  };

  return (
    <View style={styles.container}>
      <MyText text={`Point: ${point}`} />
      <RenderWheel />
      <View style={{position: 'absolute', bottom: height / 2.4}}>
        <TouchableOpacity activeOpacity={1} onPress={onPress}>
          <Image
            source={require('../assets/pointer.png')}
            style={styles.pointer}
          />
          <Image
            source={require('../assets/btn.png')}
            style={{width: 130, height: 130}}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    backgroundColor: '#fff',
  },
  pointer: {
    width: 15,
    height: 15,
    position: 'absolute',
    left: 55,
    top: 10,
  },
});
export default App;

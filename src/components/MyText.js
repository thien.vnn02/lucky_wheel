import React from 'react';
import {Text} from 'react-native';

const MyText = ({text, style}) => {
  return <Text style={style}>{text}</Text>;
};
export default MyText;
